// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.13.0
// source: v1/dictionary_md/preparative_form.proto

package dictionary_md

import (
	_ "github.com/golang/protobuf/ptypes/timestamp"
	wrappers "github.com/golang/protobuf/ptypes/wrappers"
	dictionary_common "gitlab.com/fcpartners/apis/gen/dictionary/v1/dictionary_common"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type PreparativeForm struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           int64                    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	ExternalId   int64                    `protobuf:"varint,2,opt,name=external_id,json=externalId,proto3" json:"external_id,omitempty"`
	Name         string                   `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	FullName     string                   `protobuf:"bytes,4,opt,name=full_name,json=fullName,proto3" json:"full_name,omitempty"`
	Abbreviation string                   `protobuf:"bytes,5,opt,name=abbreviation,proto3" json:"abbreviation,omitempty"`
	Description  string                   `protobuf:"bytes,6,opt,name=description,proto3" json:"description,omitempty"`
	Audit        *dictionary_common.Audit `protobuf:"bytes,21,opt,name=audit,proto3" json:"audit,omitempty"`
}

func (x *PreparativeForm) Reset() {
	*x = PreparativeForm{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PreparativeForm) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PreparativeForm) ProtoMessage() {}

func (x *PreparativeForm) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PreparativeForm.ProtoReflect.Descriptor instead.
func (*PreparativeForm) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{0}
}

func (x *PreparativeForm) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *PreparativeForm) GetExternalId() int64 {
	if x != nil {
		return x.ExternalId
	}
	return 0
}

func (x *PreparativeForm) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *PreparativeForm) GetFullName() string {
	if x != nil {
		return x.FullName
	}
	return ""
}

func (x *PreparativeForm) GetAbbreviation() string {
	if x != nil {
		return x.Abbreviation
	}
	return ""
}

func (x *PreparativeForm) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *PreparativeForm) GetAudit() *dictionary_common.Audit {
	if x != nil {
		return x.Audit
	}
	return nil
}

type PreparativeFormFilter struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id           *wrappers.Int64Value  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ExternalId   *wrappers.Int64Value  `protobuf:"bytes,2,opt,name=external_id,json=externalId,proto3" json:"external_id,omitempty"`
	Name         *wrappers.StringValue `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`                         // LIKE
	FullName     *wrappers.StringValue `protobuf:"bytes,4,opt,name=full_name,json=fullName,proto3" json:"full_name,omitempty"` // LIKE
	Abbreviation *wrappers.StringValue `protobuf:"bytes,5,opt,name=abbreviation,proto3" json:"abbreviation,omitempty"`         // LIKE
	// audit
	State     *dictionary_common.StateValue `protobuf:"bytes,21,opt,name=state,proto3" json:"state,omitempty"`
	CreatedAt *dictionary_common.TimeRange  `protobuf:"bytes,23,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	CreatedBy *wrappers.StringValue         `protobuf:"bytes,24,opt,name=created_by,json=createdBy,proto3" json:"created_by,omitempty"`
	UpdatedAt *dictionary_common.TimeRange  `protobuf:"bytes,25,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	UpdatedBy *wrappers.StringValue         `protobuf:"bytes,26,opt,name=updated_by,json=updatedBy,proto3" json:"updated_by,omitempty"`
}

func (x *PreparativeFormFilter) Reset() {
	*x = PreparativeFormFilter{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *PreparativeFormFilter) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*PreparativeFormFilter) ProtoMessage() {}

func (x *PreparativeFormFilter) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use PreparativeFormFilter.ProtoReflect.Descriptor instead.
func (*PreparativeFormFilter) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{1}
}

func (x *PreparativeFormFilter) GetId() *wrappers.Int64Value {
	if x != nil {
		return x.Id
	}
	return nil
}

func (x *PreparativeFormFilter) GetExternalId() *wrappers.Int64Value {
	if x != nil {
		return x.ExternalId
	}
	return nil
}

func (x *PreparativeFormFilter) GetName() *wrappers.StringValue {
	if x != nil {
		return x.Name
	}
	return nil
}

func (x *PreparativeFormFilter) GetFullName() *wrappers.StringValue {
	if x != nil {
		return x.FullName
	}
	return nil
}

func (x *PreparativeFormFilter) GetAbbreviation() *wrappers.StringValue {
	if x != nil {
		return x.Abbreviation
	}
	return nil
}

func (x *PreparativeFormFilter) GetState() *dictionary_common.StateValue {
	if x != nil {
		return x.State
	}
	return nil
}

func (x *PreparativeFormFilter) GetCreatedAt() *dictionary_common.TimeRange {
	if x != nil {
		return x.CreatedAt
	}
	return nil
}

func (x *PreparativeFormFilter) GetCreatedBy() *wrappers.StringValue {
	if x != nil {
		return x.CreatedBy
	}
	return nil
}

func (x *PreparativeFormFilter) GetUpdatedAt() *dictionary_common.TimeRange {
	if x != nil {
		return x.UpdatedAt
	}
	return nil
}

func (x *PreparativeFormFilter) GetUpdatedBy() *wrappers.StringValue {
	if x != nil {
		return x.UpdatedBy
	}
	return nil
}

type GetPreparativeFormRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GetPreparativeFormRequest) Reset() {
	*x = GetPreparativeFormRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetPreparativeFormRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetPreparativeFormRequest) ProtoMessage() {}

func (x *GetPreparativeFormRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetPreparativeFormRequest.ProtoReflect.Descriptor instead.
func (*GetPreparativeFormRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{2}
}

func (x *GetPreparativeFormRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

type GetPreparativeFormResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Item *PreparativeForm `protobuf:"bytes,1,opt,name=item,proto3" json:"item,omitempty"`
}

func (x *GetPreparativeFormResponse) Reset() {
	*x = GetPreparativeFormResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetPreparativeFormResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetPreparativeFormResponse) ProtoMessage() {}

func (x *GetPreparativeFormResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetPreparativeFormResponse.ProtoReflect.Descriptor instead.
func (*GetPreparativeFormResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{3}
}

func (x *GetPreparativeFormResponse) GetItem() *PreparativeForm {
	if x != nil {
		return x.Item
	}
	return nil
}

type ListPreparativeFormRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Filter *PreparativeFormFilter `protobuf:"bytes,1,opt,name=filter,proto3" json:"filter,omitempty"`
	// sortable fields: id, external_id, name, full_name, abbreviation, created_at, updated_at
	Sorting []*dictionary_common.Sorting `protobuf:"bytes,2,rep,name=sorting,proto3" json:"sorting,omitempty"`
	// Pagination - Optional
	Pagination *dictionary_common.PaginationRequest `protobuf:"bytes,3,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *ListPreparativeFormRequest) Reset() {
	*x = ListPreparativeFormRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListPreparativeFormRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListPreparativeFormRequest) ProtoMessage() {}

func (x *ListPreparativeFormRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListPreparativeFormRequest.ProtoReflect.Descriptor instead.
func (*ListPreparativeFormRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{4}
}

func (x *ListPreparativeFormRequest) GetFilter() *PreparativeFormFilter {
	if x != nil {
		return x.Filter
	}
	return nil
}

func (x *ListPreparativeFormRequest) GetSorting() []*dictionary_common.Sorting {
	if x != nil {
		return x.Sorting
	}
	return nil
}

func (x *ListPreparativeFormRequest) GetPagination() *dictionary_common.PaginationRequest {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type ListPreparativeFormResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Items      []*PreparativeForm                    `protobuf:"bytes,1,rep,name=items,proto3" json:"items,omitempty"`
	Pagination *dictionary_common.PaginationResponse `protobuf:"bytes,2,opt,name=pagination,proto3" json:"pagination,omitempty"`
}

func (x *ListPreparativeFormResponse) Reset() {
	*x = ListPreparativeFormResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListPreparativeFormResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListPreparativeFormResponse) ProtoMessage() {}

func (x *ListPreparativeFormResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListPreparativeFormResponse.ProtoReflect.Descriptor instead.
func (*ListPreparativeFormResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{5}
}

func (x *ListPreparativeFormResponse) GetItems() []*PreparativeForm {
	if x != nil {
		return x.Items
	}
	return nil
}

func (x *ListPreparativeFormResponse) GetPagination() *dictionary_common.PaginationResponse {
	if x != nil {
		return x.Pagination
	}
	return nil
}

type SavePreparativeFormRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Item *PreparativeForm `protobuf:"bytes,1,opt,name=item,proto3" json:"item,omitempty"`
}

func (x *SavePreparativeFormRequest) Reset() {
	*x = SavePreparativeFormRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SavePreparativeFormRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SavePreparativeFormRequest) ProtoMessage() {}

func (x *SavePreparativeFormRequest) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SavePreparativeFormRequest.ProtoReflect.Descriptor instead.
func (*SavePreparativeFormRequest) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{6}
}

func (x *SavePreparativeFormRequest) GetItem() *PreparativeForm {
	if x != nil {
		return x.Item
	}
	return nil
}

type SavePreparativeFormResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *SavePreparativeFormResponse) Reset() {
	*x = SavePreparativeFormResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SavePreparativeFormResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SavePreparativeFormResponse) ProtoMessage() {}

func (x *SavePreparativeFormResponse) ProtoReflect() protoreflect.Message {
	mi := &file_v1_dictionary_md_preparative_form_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SavePreparativeFormResponse.ProtoReflect.Descriptor instead.
func (*SavePreparativeFormResponse) Descriptor() ([]byte, []int) {
	return file_v1_dictionary_md_preparative_form_proto_rawDescGZIP(), []int{7}
}

func (x *SavePreparativeFormResponse) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

var File_v1_dictionary_md_preparative_form_proto protoreflect.FileDescriptor

var file_v1_dictionary_md_preparative_form_proto_rawDesc = []byte{
	0x0a, 0x27, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f,
	0x6d, 0x64, 0x2f, 0x70, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x5f, 0x66,
	0x6f, 0x72, 0x6d, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1f, 0x66, 0x63, 0x70, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0x1a, 0x1e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x77, 0x72, 0x61, 0x70,
	0x70, 0x65, 0x72, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65,
	0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x2c, 0x76, 0x31, 0x2f,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2f, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x81, 0x02, 0x0a, 0x0f, 0x50, 0x72,
	0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1f, 0x0a,
	0x0b, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x0a, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x49, 0x64, 0x12, 0x12,
	0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61,
	0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x66, 0x75, 0x6c, 0x6c, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x66, 0x75, 0x6c, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x22, 0x0a, 0x0c, 0x61, 0x62, 0x62, 0x72, 0x65, 0x76, 0x69, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x61, 0x62, 0x62, 0x72, 0x65, 0x76, 0x69, 0x61, 0x74,
	0x69, 0x6f, 0x6e, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69,
	0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x40, 0x0a, 0x05, 0x61, 0x75, 0x64, 0x69, 0x74, 0x18, 0x15,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x2a, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69,
	0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x41, 0x75, 0x64, 0x69, 0x74,
	0x52, 0x05, 0x61, 0x75, 0x64, 0x69, 0x74, 0x4a, 0x04, 0x08, 0x07, 0x10, 0x15, 0x22, 0x9c, 0x05,
	0x0a, 0x15, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72,
	0x6d, 0x46, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x2b, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x49, 0x6e, 0x74, 0x36, 0x34, 0x56, 0x61, 0x6c, 0x75, 0x65,
	0x52, 0x02, 0x69, 0x64, 0x12, 0x3c, 0x0a, 0x0b, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c,
	0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1b, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x49, 0x6e, 0x74, 0x36,
	0x34, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x0a, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c,
	0x49, 0x64, 0x12, 0x30, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x04,
	0x6e, 0x61, 0x6d, 0x65, 0x12, 0x39, 0x0a, 0x09, 0x66, 0x75, 0x6c, 0x6c, 0x5f, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67,
	0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x08, 0x66, 0x75, 0x6c, 0x6c, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x40, 0x0a, 0x0c, 0x61, 0x62, 0x62, 0x72, 0x65, 0x76, 0x69, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x18,
	0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67, 0x56, 0x61,
	0x6c, 0x75, 0x65, 0x52, 0x0c, 0x61, 0x62, 0x62, 0x72, 0x65, 0x76, 0x69, 0x61, 0x74, 0x69, 0x6f,
	0x6e, 0x12, 0x45, 0x0a, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x18, 0x15, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x2f, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72,
	0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f,
	0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x53, 0x74, 0x61, 0x74, 0x65, 0x56, 0x61, 0x6c, 0x75,
	0x65, 0x52, 0x05, 0x73, 0x74, 0x61, 0x74, 0x65, 0x12, 0x4d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x17, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2e, 0x2e, 0x66,
	0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x52, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x09, 0x63, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x3b, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x5f, 0x62, 0x79, 0x18, 0x18, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f,
	0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74,
	0x72, 0x69, 0x6e, 0x67, 0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x42, 0x79, 0x12, 0x4d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f,
	0x61, 0x74, 0x18, 0x19, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x2e, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x54,
	0x69, 0x6d, 0x65, 0x52, 0x61, 0x6e, 0x67, 0x65, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x64, 0x41, 0x74, 0x12, 0x3b, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x62,
	0x79, 0x18, 0x1a, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x69, 0x6e, 0x67,
	0x56, 0x61, 0x6c, 0x75, 0x65, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64, 0x42, 0x79,
	0x4a, 0x04, 0x08, 0x06, 0x10, 0x15, 0x4a, 0x04, 0x08, 0x16, 0x10, 0x17, 0x22, 0x2b, 0x0a, 0x19,
	0x47, 0x65, 0x74, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f,
	0x72, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x22, 0x62, 0x0a, 0x1a, 0x47, 0x65, 0x74,
	0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x44, 0x0a, 0x04, 0x69, 0x74, 0x65, 0x6d, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x30, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74,
	0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0x2e, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74,
	0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x52, 0x04, 0x69, 0x74, 0x65, 0x6d, 0x22, 0x8c, 0x02,
	0x0a, 0x1a, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76,
	0x65, 0x46, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x4e, 0x0a, 0x06,
	0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x36, 0x2e, 0x66,
	0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0x2e, 0x50,
	0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x46, 0x69,
	0x6c, 0x74, 0x65, 0x72, 0x52, 0x06, 0x66, 0x69, 0x6c, 0x74, 0x65, 0x72, 0x12, 0x46, 0x0a, 0x07,
	0x73, 0x6f, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2c, 0x2e,
	0x66, 0x63, 0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76,
	0x31, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d,
	0x6d, 0x6f, 0x6e, 0x2e, 0x53, 0x6f, 0x72, 0x74, 0x69, 0x6e, 0x67, 0x52, 0x07, 0x73, 0x6f, 0x72,
	0x74, 0x69, 0x6e, 0x67, 0x12, 0x56, 0x0a, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x36, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50,
	0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x52, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0xbe, 0x01, 0x0a,
	0x1b, 0x4c, 0x69, 0x73, 0x74, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65,
	0x46, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x46, 0x0a, 0x05,
	0x69, 0x74, 0x65, 0x6d, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x30, 0x2e, 0x66, 0x63,
	0x70, 0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0x2e, 0x50, 0x72,
	0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x52, 0x05, 0x69,
	0x74, 0x65, 0x6d, 0x73, 0x12, 0x57, 0x0a, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69,
	0x6f, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x37, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x50,
	0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73,
	0x65, 0x52, 0x0a, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x22, 0x62, 0x0a,
	0x1a, 0x53, 0x61, 0x76, 0x65, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65,
	0x46, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x44, 0x0a, 0x04, 0x69,
	0x74, 0x65, 0x6d, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x30, 0x2e, 0x66, 0x63, 0x70, 0x2e,
	0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64, 0x69,
	0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0x2e, 0x50, 0x72, 0x65, 0x70,
	0x61, 0x72, 0x61, 0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x52, 0x04, 0x69, 0x74, 0x65,
	0x6d, 0x22, 0x2d, 0x0a, 0x1b, 0x53, 0x61, 0x76, 0x65, 0x50, 0x72, 0x65, 0x70, 0x61, 0x72, 0x61,
	0x74, 0x69, 0x76, 0x65, 0x46, 0x6f, 0x72, 0x6d, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64,
	0x42, 0x6b, 0x0a, 0x28, 0x63, 0x6f, 0x6d, 0x2e, 0x66, 0x63, 0x70, 0x2e, 0x61, 0x70, 0x69, 0x73,
	0x2e, 0x64, 0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2e, 0x76, 0x31, 0x2e, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0x50, 0x01, 0x5a, 0x3a,
	0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x66, 0x63, 0x70, 0x61, 0x72,
	0x74, 0x6e, 0x65, 0x72, 0x73, 0x2f, 0x61, 0x70, 0x69, 0x73, 0x2f, 0x67, 0x65, 0x6e, 0x2f, 0x64,
	0x69, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x2f, 0x76, 0x31, 0x2f, 0x64, 0x69, 0x63,
	0x74, 0x69, 0x6f, 0x6e, 0x61, 0x72, 0x79, 0x5f, 0x6d, 0x64, 0xba, 0x02, 0x00, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_v1_dictionary_md_preparative_form_proto_rawDescOnce sync.Once
	file_v1_dictionary_md_preparative_form_proto_rawDescData = file_v1_dictionary_md_preparative_form_proto_rawDesc
)

func file_v1_dictionary_md_preparative_form_proto_rawDescGZIP() []byte {
	file_v1_dictionary_md_preparative_form_proto_rawDescOnce.Do(func() {
		file_v1_dictionary_md_preparative_form_proto_rawDescData = protoimpl.X.CompressGZIP(file_v1_dictionary_md_preparative_form_proto_rawDescData)
	})
	return file_v1_dictionary_md_preparative_form_proto_rawDescData
}

var file_v1_dictionary_md_preparative_form_proto_msgTypes = make([]protoimpl.MessageInfo, 8)
var file_v1_dictionary_md_preparative_form_proto_goTypes = []interface{}{
	(*PreparativeForm)(nil),                      // 0: fcp.dictionary.v1.dictionary_md.PreparativeForm
	(*PreparativeFormFilter)(nil),                // 1: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter
	(*GetPreparativeFormRequest)(nil),            // 2: fcp.dictionary.v1.dictionary_md.GetPreparativeFormRequest
	(*GetPreparativeFormResponse)(nil),           // 3: fcp.dictionary.v1.dictionary_md.GetPreparativeFormResponse
	(*ListPreparativeFormRequest)(nil),           // 4: fcp.dictionary.v1.dictionary_md.ListPreparativeFormRequest
	(*ListPreparativeFormResponse)(nil),          // 5: fcp.dictionary.v1.dictionary_md.ListPreparativeFormResponse
	(*SavePreparativeFormRequest)(nil),           // 6: fcp.dictionary.v1.dictionary_md.SavePreparativeFormRequest
	(*SavePreparativeFormResponse)(nil),          // 7: fcp.dictionary.v1.dictionary_md.SavePreparativeFormResponse
	(*dictionary_common.Audit)(nil),              // 8: fcp.dictionary.v1.dictionary_common.Audit
	(*wrappers.Int64Value)(nil),                  // 9: google.protobuf.Int64Value
	(*wrappers.StringValue)(nil),                 // 10: google.protobuf.StringValue
	(*dictionary_common.StateValue)(nil),         // 11: fcp.dictionary.v1.dictionary_common.StateValue
	(*dictionary_common.TimeRange)(nil),          // 12: fcp.dictionary.v1.dictionary_common.TimeRange
	(*dictionary_common.Sorting)(nil),            // 13: fcp.dictionary.v1.dictionary_common.Sorting
	(*dictionary_common.PaginationRequest)(nil),  // 14: fcp.dictionary.v1.dictionary_common.PaginationRequest
	(*dictionary_common.PaginationResponse)(nil), // 15: fcp.dictionary.v1.dictionary_common.PaginationResponse
}
var file_v1_dictionary_md_preparative_form_proto_depIdxs = []int32{
	8,  // 0: fcp.dictionary.v1.dictionary_md.PreparativeForm.audit:type_name -> fcp.dictionary.v1.dictionary_common.Audit
	9,  // 1: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.id:type_name -> google.protobuf.Int64Value
	9,  // 2: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.external_id:type_name -> google.protobuf.Int64Value
	10, // 3: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.name:type_name -> google.protobuf.StringValue
	10, // 4: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.full_name:type_name -> google.protobuf.StringValue
	10, // 5: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.abbreviation:type_name -> google.protobuf.StringValue
	11, // 6: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.state:type_name -> fcp.dictionary.v1.dictionary_common.StateValue
	12, // 7: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.created_at:type_name -> fcp.dictionary.v1.dictionary_common.TimeRange
	10, // 8: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.created_by:type_name -> google.protobuf.StringValue
	12, // 9: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.updated_at:type_name -> fcp.dictionary.v1.dictionary_common.TimeRange
	10, // 10: fcp.dictionary.v1.dictionary_md.PreparativeFormFilter.updated_by:type_name -> google.protobuf.StringValue
	0,  // 11: fcp.dictionary.v1.dictionary_md.GetPreparativeFormResponse.item:type_name -> fcp.dictionary.v1.dictionary_md.PreparativeForm
	1,  // 12: fcp.dictionary.v1.dictionary_md.ListPreparativeFormRequest.filter:type_name -> fcp.dictionary.v1.dictionary_md.PreparativeFormFilter
	13, // 13: fcp.dictionary.v1.dictionary_md.ListPreparativeFormRequest.sorting:type_name -> fcp.dictionary.v1.dictionary_common.Sorting
	14, // 14: fcp.dictionary.v1.dictionary_md.ListPreparativeFormRequest.pagination:type_name -> fcp.dictionary.v1.dictionary_common.PaginationRequest
	0,  // 15: fcp.dictionary.v1.dictionary_md.ListPreparativeFormResponse.items:type_name -> fcp.dictionary.v1.dictionary_md.PreparativeForm
	15, // 16: fcp.dictionary.v1.dictionary_md.ListPreparativeFormResponse.pagination:type_name -> fcp.dictionary.v1.dictionary_common.PaginationResponse
	0,  // 17: fcp.dictionary.v1.dictionary_md.SavePreparativeFormRequest.item:type_name -> fcp.dictionary.v1.dictionary_md.PreparativeForm
	18, // [18:18] is the sub-list for method output_type
	18, // [18:18] is the sub-list for method input_type
	18, // [18:18] is the sub-list for extension type_name
	18, // [18:18] is the sub-list for extension extendee
	0,  // [0:18] is the sub-list for field type_name
}

func init() { file_v1_dictionary_md_preparative_form_proto_init() }
func file_v1_dictionary_md_preparative_form_proto_init() {
	if File_v1_dictionary_md_preparative_form_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_v1_dictionary_md_preparative_form_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PreparativeForm); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*PreparativeFormFilter); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetPreparativeFormRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetPreparativeFormResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListPreparativeFormRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListPreparativeFormResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SavePreparativeFormRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_v1_dictionary_md_preparative_form_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SavePreparativeFormResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_v1_dictionary_md_preparative_form_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   8,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_v1_dictionary_md_preparative_form_proto_goTypes,
		DependencyIndexes: file_v1_dictionary_md_preparative_form_proto_depIdxs,
		MessageInfos:      file_v1_dictionary_md_preparative_form_proto_msgTypes,
	}.Build()
	File_v1_dictionary_md_preparative_form_proto = out.File
	file_v1_dictionary_md_preparative_form_proto_rawDesc = nil
	file_v1_dictionary_md_preparative_form_proto_goTypes = nil
	file_v1_dictionary_md_preparative_form_proto_depIdxs = nil
}
